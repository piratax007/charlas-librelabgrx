# Charlas LibreLabGRX

Este es un documento abierto donde puedes proponer charlas para realizar a lo largo del año. Vamos a hacer un llamamiento especial para emitir mini-charlas (de menos de 30 minutos) a través de (https://meet.jit.si).

Serán todos los días, a partir de las 12:30 de la mañana, y trataremos de llenar cada día con una.

## Instrucciones

* Haz un fork de este repositorio (o edítalo directamente desde la web)
* Crea un documento describiendo tu charla y tu persona.
* Haz un pull request (desde la web lo puedes hacer directamente).
* Espera a que la organización acepte el pull request
* ¡Charla aceptada!

## Charlas LibreLab@Home

Añade tu charla aquí abajo. Pon simplemente tu nombre, de qué quieres hablar, y de qué día. Se emitirán interactivamente usando [Jitsi Meet](https://meet.jit.si/LibreLabGRXEnCasa).

* 19 de marzo: [JJ Merelo](https://github.com): *Raku, el nuevo lenguaje del que todo el mundo debería estar hablando*.


## Charlas propuestas

* [Gestión de citas bibliográficas con Zotero](https://gitlab.com/Prosumidor/charlas-librelabgrx/blob/9bbeded05dad7fcb5e9e50296ac46a3d45e26485/2019-20-1/Charla%20Zotero.md).
* [Visualización de redes sociales con Gephi](https://gitlab.com/Prosumidor/charlas-librelabgrx/blob/9bbeded05dad7fcb5e9e50296ac46a3d45e26485/2019-20-1/Charla%20Gephi.md).
* [Añade aquí tu charla y enlaza el documento](2019-20-1/plantilla.md).
